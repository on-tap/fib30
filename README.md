# fib30 [![Generated with](https://img.shields.io/badge/generated%20with-bangular-blue.svg?style=flat-square)](https://github.com/42Zavattas/generator-bangular)


## Getting Started

1. Run `npm install` to install node packages.

2. Run `bower install` to install bower dependencies.

3. Run `npm start` to kick off store daemon and run server.js

## Testing

Running `npm test` will run the unit tests with karma.
