'use strict';

describe('Controller: MainCtrl', function () {

  beforeEach(module('fib30'));

  var MainCtrl;

  beforeEach(inject(function ($controller) {
    MainCtrl = $controller('MainCtrl', {});
  }));

  it('should ...', function () {
    expect(1).toBe(1);
  });

});
