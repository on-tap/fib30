'use strict';

angular.module('fib30').controller('MainCtrl', ['$scope', 'Numbers',
  function ($scope, Numbers) {

    function errorHandler (error) {
      console.error(error);
      $scope.error = true;
    }
    var vm = this;
    // $scope.numberHistory = [1,2];

    Numbers.getFibonacci30().then(function (fib30) {
      $scope.error = !Array.isArray(fib30);
      $scope.fibonacci30 = fib30;
    }).catch(errorHandler);

    $scope.postNumber = function (number) {
      Numbers.postNumber(number * 1).then(function (success) {
        // Only updating history after successful write.
        $scope.error = !success;
        var num = vm.numberHistory || [];
        num.push(number * 1);
        vm.numberHistory = num;
        $scope.getTotal();
      }).catch(errorHandler);
    };

    $scope.getTotal = function () {
      Numbers.getTotal().then(function (total) {
        vm.total = total * 1;
      }).catch(errorHandler);
    };

  }]);
