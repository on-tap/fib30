'use strict';

angular.module('fib30').service('Numbers', [ '$http', function ($http) {
  return {
    getFibonacci30: function () {
      return $http.get('/api/numbers/fibonacci').then(function (response) {
        return response && response.status === 200 && response.data && response.data.body;
      });
    },
    postNumber: function (number) {
      return $http.post('/api/numbers/', { number: number }).then(function (response) {
        return response && response.status === 200;
      });
    },
    getTotal: function () {
      return $http.get('/api/numbers/').then(function (response) {
        return response && response.status === 200 && response.data && response.data.body;
      });
    }
  };
} ]);
