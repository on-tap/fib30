'use strict';
angular.module('fib30', ['ngRoute', 'ngTouch', 'ngCookies']).config(function ($routeProvider, $locationProvider) {
  $routeProvider.otherwise({
    redirectTo: function () {
      return '/';
    }
  });
  $locationProvider.html5Mode(true);
}).run(['$rootScope',
  function ($rootScope) {
  }]);
