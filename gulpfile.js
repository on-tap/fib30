'use strict';

var gulp = require('gulp');

process.env.NODE_ENV = process.env.NODE_ENV || 'local';

gulp.task('default',    ['serve']);
gulp.task('nodemon',    ['watch'],    require('./tasks/serve').nodemon);
gulp.task('serve',      ['nodemon'],  require('./tasks/serve').bsync);
gulp.task('watch',      ['inject'],   require('./tasks/watch'));
gulp.task('inject',     ['sass'],     require('./tasks/inject'));
gulp.task('sass',                     require('./tasks/sass'));
gulp.task('preview',    ['build'],    require('./tasks/preview'));
gulp.task('build',      ['control'],              require('./tasks/build'));
gulp.task('bump',                     require('./tasks/version').bump);
gulp.task('control',                  require('./tasks/control'));
gulp.task('e2e:update',               require('./tasks/test').e2eUpdate);
gulp.task('e2e',        ['serve'],    require('./tasks/test').e2eTests);
gulp.task('test',                     require('./tasks/test').test);
gulp.task('sassdoc',                  require('./tasks/doc').sassdoc);
gulp.task('apidoc',                   require('./tasks/doc').apidoc);
