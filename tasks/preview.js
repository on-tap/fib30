'use strict';

/**
 * Preview the build app
 */

module.exports = function () {
  var gulp = require('gulp');
  var open = require('gulp-open');

  var config = require('../dist/config/environment');

  var openOpts = {
    url: 'http://localhost:' + config.port,
    already: false
  };

  process.env.NODE_ENV = 'production';
  require('../dist/server.js');
  return gulp.src('dist/public/index.html')
    .pipe(open('', openOpts));
};
