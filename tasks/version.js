'use strict';

/**
 * Git versioning and bump
 */

var gulp = require('gulp');
var fs   = require('fs');
var bump = require('gulp-bump');

module.exports = {
  bump: function () {
    gulp.task('bump', function () {
      gulp.src([ './version.json', './package.json', './bower.json' ])
        .pipe(bump())
        .pipe(gulp.dest('./'));
    });
  },
  buildVersion: function () {
    var versionJson = JSON.parse(fs.readFileSync('version.json', 'utf8'));
    var jenkinsBuildName = process.env.CURRENT_BUILD_ZIP || 'not-built-on-jenkins-' + versionJson.version;
    if (versionJson) {
      versionJson.released = jenkinsBuildName;
      versionJson.released_date = new Date();
      fs.writeFile('version.json', JSON.stringify(versionJson), function (err) {
        if (err) {
          throw err;
        }
      });
    }
  }
};
