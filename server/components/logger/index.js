'use strict';
var winston = require('winston');
var path = require('path');
var PROJECT_ROOT = path.join(__dirname, '..');
var config = require('../../config/environment');
var fs = require('fs');
var dir = PROJECT_ROOT + '/../logs';

winston.emitErrs = true;

// NOTE: this adds a filename and line number to winston's output
// Example output: 'info (routes/index.js:34) GET 200 /index'
/**
 * Parses and returns info about the call stack at the given index.
 */
function getStackInfo (stackIndex) {
  // get call stack, and analyze it
  // get all file, method, and line numbers
  var stacklist = (new Error()).stack.split('\n').slice(3);

  // stack trace format:
  // http://code.google.com/p/v8/wiki/JavaScriptStackTraceApi
  // do not remove the regex expresses to outside of this method (due to a BUG in node.js)
  var stackReg = /at\s+(.*)\s+\((.*):(\d*):(\d*)\)/gi;
  var stackReg2 = /at\s+()(.*):(\d*):(\d*)/gi;

  var s = stacklist[ stackIndex ] || stacklist[ 0 ];
  var sp = stackReg.exec(s) || stackReg2.exec(s);

  if (sp && sp.length === 5) {
    return {
      method: sp[ 1 ],
      relativePath: path.relative(PROJECT_ROOT, sp[ 2 ]),
      line: sp[ 3 ],
      pos: sp[ 4 ],
      file: path.basename(sp[ 2 ]),
      stack: stacklist.join('\n')
    };
  }
}
/**
 * Attempts to add file and line number info to the given log arguments.
 */
function formatLogArguments (args) {
  args = Array.prototype.slice.call(args);

  var stackInfo = getStackInfo(1);

  if (stackInfo) {
    // get file path relative to project root
    var calleeStr = '(' + stackInfo.relativePath + ':' + stackInfo.line + ')';

    if (typeof (args[ 0 ]) === 'string') {
      args[ 0 ] = calleeStr + ' ' + args[ 0 ];
    } else {
      args.unshift(calleeStr);
    }
  }
  return args;
}

var logger = new winston.Logger({
  transports: [
    new winston.transports.File({
    level: config.LOG_LEVEL,
    filename: dir + '/' + config.LOG_FILE_NAME + '.log',
    handleExceptions: true,
    json: true,
    maxsize: 5242880, //5M
    maxFiles: 5,
    colorize: false
  }),
    new winston.transports.Console({
    level: config.LOG_LEVEL, handleExceptions: true, json: false, colorize: true, prettyPrint: true, timestamp: true
  }) ], exitOnError: false
});

// this allows winston to handle output from express' morgan middleware
logger.stream = {
  write: function (message) {
    logger.info(message);
  }
};

if (config.NODE_ENV !== 'production') {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  // A custom logger interface that wraps winston, making it easy to instrument
  // code and still possible to replace winston in the future.
  module.exports.debug = module.exports.log = function () {
    logger.debug.apply(logger, formatLogArguments(arguments));
  };

  module.exports.info = function () {
    logger.info.apply(logger, formatLogArguments(arguments));
  };

  module.exports.warn = function () {
    logger.warn.apply(logger, formatLogArguments(arguments));
  };

  module.exports.error = function () {
    logger.error.apply(logger, formatLogArguments(arguments));
  };

  module.exports.stream = logger.stream;

} else {
  var boringLog = function () {
    console.log(arguments);
  };
  module.exports.debug = boringLog;
  module.exports.info = boringLog;
  module.exports.warn = boringLog;
  module.exports.error = boringLog;
  module.exports.stream = boringLog;
}

