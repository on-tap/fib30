const http = require("http");
const config = require('../../config/environment');
let total = 0;

const server = http.createServer((req, res) => {
  let body = '';
  req.setEncoding('utf8');

  req.on('data', (chunk) => {
    body += chunk;
  });

  req.on('end', () => {
    try {
      if (body) {
        const data = JSON.parse(body);
        if (typeof data.add === 'number' && data.add) {
          total += data.add;
        } else {
          total = -1;
        }
      }
      res.write(String(total));
      res.end();
    } catch (er) {
      res.statusCode = 400;
      return res.end(`error: ${er.message}`);
    }
  });
});

server.listen(config.store.port);
