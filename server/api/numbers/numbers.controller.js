'use strict';

const fetchUrl = require('fetch').fetchUrl;
const config = require('../../config/environment');
/**
 * API REF:
 * GET     /api/things              ->  index
 * POST    /api/things              ->  create
 * GET     /api/things/:id          ->  show
 *
 */

function index (req, res) {
  fetchUrl(`${config.store.host}:${config.store.port}`, { method: 'GET' }, function(error, meta, body){
    if (error) {
      res.statusCode(502);
      res.setHeader('Cache-Control', 'public, max-age=0');
    }
    res.setHeader('Cache-Control', 'public, max-age=60');
    return res.json({
      error,
      body: body && body.toString()
    });
  });

}

function create (req, res) {
  const number = req && req.body && req.body.number;
  fetchUrl(`${config.store.host}:${config.store.port}`, { method: 'POST', payload: JSON.stringify({ add: number }) }, function(error, meta, body){
    res.setHeader('Cache-Control', 'public, max-age=0');
    return res.json({
      error,
      body: body && body.toString()
    });
  });
}

function show (req, res) {
  res.setHeader('Cache-Control', 'public, max-age=2592000000');
  /*
  *
  *   TODO: Testing: Add unit tests, integration tests specifically with API and daemon store
  *   TODO: Design: Add a header with a proper looking drop-down
  *   TODO: UX: Add a filter to the fib list dropdown
  *
  * */
  return res.json({
    body: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584,
      4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040]
  });
}

module.exports = {
  index,
  create,
  show
};
