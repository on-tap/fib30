'use strict';

const express = require('express');
const controller = require('./numbers.controller');

const router = express.Router();

router.get('/', controller.index);
router.post('/', controller.create);
router.get('/:type', controller.show);

module.exports = router;
