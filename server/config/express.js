'use strict';

var express = require('express');
var morgan = require('morgan');
var path = require('path');
var bodyParser = require('body-parser');
var config = require('./environment');

module.exports = function (app) {
  var env = config.env;
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  if (env === 'production') {
    app.use(express.static(path.join(config.root, '/public')));
    app.set('views', config.root + '/views');
    app.set('appPath', 'public');
    app.use(morgan('combined', {
      skip: function (req, res) {
        return res.statusCode > 400;
      }
    }));
  } else if (env === 'qa' || env === 'build') {
    app.use(express.static(path.join(config.root, '/public')));
    app.set('views', config.root + '/views');
    app.set('appPath', 'public');
    app.use(morgan('dev'));
  } else {
    app.use(express.static(path.join(config.root, '/client')));
    app.set('views', config.root + '/server/views');
    app.use(require('errorhandler')());
    app.set('appPath', 'client');
    app.use(morgan('dev'));
  }
};
