'use strict';
var path = require('path');

module.exports = {
  ip: process.env.IP || 7002,
  root: path.normalize(__dirname + '/../..'),
  NODE_ENV: 'qa',
  LOG_LEVEL: 'debug'
};

