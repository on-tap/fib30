'use strict';
var path = require('path');

module.exports = {
  port: process.env.PORT || 7000,
  root: path.normalize(__dirname + '/../..'),
  NODE_ENV: 'production',
  LOG_LEVEL: 'info'
};
