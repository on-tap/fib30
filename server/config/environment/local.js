'use strict';

module.exports = {
  port: process.env.PORT || 9000,
  NODE_ENV: 'local',
  LOG_LEVEL: 'debug'
};
