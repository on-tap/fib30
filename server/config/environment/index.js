'use strict';

var path = require('path');
var _ = require('lodash');
var all = {
  env: process.env.NODE_ENV || 'local',
  root: process.env.root || path.normalize(__dirname + '/../../..'),
  port: process.env.PORT || 9000,
  LOG_FILE_NAME: 'fibonacci-30',
  store: {
    host: 'http://localhost',
    port: 1337
  }
};

module.exports = _.merge(all, require('./' + all.env + '.js'));
