'use strict';
var path = require('path');

module.exports = {
  port: process.env.PORT || 7001,
  root: path.normalize(__dirname + '/../..'),
  NODE_ENV: 'build',
  LOG_LEVEL: 'debug'
};
