'use strict';

var config = require('./config/environment');
var express = require('express');

var app = express();
var server = require('http').createServer(app);

require('./config/express')(app);
require('./routes')(app);

server.listen(config.port, config.ip, function () {
  console.log('NODE_ENV =' + config.env);
  console.log('\nExpress server listening on port ' + config.port + ' in ' + app.get('env') + ' environment.');
  if (config.env === 'local') {
    require('ripe').ready();
  }
});

module.exports = server;
