'use strict';

var config = require('./config/environment');
var errors = require('./components/errors');

module.exports = function (app) {
  // API
  app.use('/api/numbers', require('./api/numbers'));

  app.route('/').get(function (req, res) {
    res.sendFile(app.get('appPath') + '/index.html', { root: config.root });
  });

  app.route('*').get(errors[ 404 ]);
};
